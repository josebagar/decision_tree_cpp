Instrucciones de compilación módulos en Windows (64bits)
========================================================

  * Instalar la versión de 64 bits de [MSYS2](http://msys2.github.io/) (seguir las instrucciones de la web, incluyendo la actualización completa de los paquetes).
  * Abrir *MinGW-w64 Win64 Shell* (**la que pone Win64, no la de Win32**)
  * Instalar las herramientas de línea de comandos necesarias:
```
pacman -S msys/openssh msys/binutils msys/git msys/tar msys/gzip msys/bzip2 msys/xz mingw64/mingw-w64-x86_64-cmake mingw64/mingw-w64-x86_64-ninja mingw64/mingw-w64-x86_64-clang
```
  * Instalar las dependencias de los paquetes:
```
pacman -S mingw64/mingw-w64-x86_64-armadillo mingw64/mingw-w64-x86_64-openblas
```
  * Ir al directorio donde se encuentra el código fuente del módulo y generar una carpeta `build`:

    *OjoCuidao* El siguiente comando asume que el código fuente del repositorio `misc` se encuentra en la carpeta de inicio de MinGW64, lo cual generalmente no será el caso.
```
cd misc/C++/pybind11/decision_tree
mkdir build
cd build
```
  * Generar los ficheros de proyecto, apuntando a la ruta correcta para la instalación del entorno Anaconda correspondiente.

    *OjoCuidao* La ruta que se incluye en el comando es la que vale en el ordenador del ejemplo, hay que adaptarla teniendo en cuenta la ruta al entorno Anaconda y hay que usar barras tipo Unix "/" y una ruta tipo mingw (`C:\Anaconda3\envs` => `/c/Anaconda3/envs`).
```
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DPYTHON_LIBRARY=/c/Anaconda3/envs/py35/python35.dll -DPYTHON_INCLUDE_DIR=/c/Anaconda3/envs/py35/include ..
```
  * Compilar:
```
ninja
```

Instrucciones de compilación módulos en Ubuntu 14.04.4 (64bits)
===============================================================
