#include <iostream>
#define ARMA_64BIT_WORD
#include <armadillo>
#include "pybind11/pybind11.h"
#include "pybind11/numpy.h"
#include "pybind11/stl.h"
#include "json/json.h"
#include <omp.h>

#include "_decision_tree.h"

namespace _decision_tree {
    using namespace arma;
    using json = nlohmann::json;

    /*
     * Shannon entropy for the given probability distribution.
     *
     * The magnitude returned by this function represents how dispersed the points
     * in the distribution are.
     * The entropy values go 0-log(n), with n being the number of elements in the
     * distribution.
    */
    double shannonEntropy(const Col<double> &probs) {
        return -sum(probs % log2(probs));
    }

    /*
     * Return a column vector with the proportions in which each of
     * the values in outcomes is present.
     * If outcomes is:
     *      {0, 1, 1, 1, 2, 2}
     * then this function will return a column vector with values:
     *      {⅙, 0.5, ⅓}ᵀ
    */
    Col<double> probs_distribution(const Col<uword> &outcomes) {
        // Get a sorted vector with all the possible outcomes
        Col<uword> possible_outcomes = unique(outcomes);
        Col<double> probs = vec(possible_outcomes.n_elem);

        // Compute relative number of entries
        for(uword i=0; i < possible_outcomes.n_elem; i++) {
            Col<uword> found = find(outcomes == possible_outcomes[i]);
            probs[i] =   static_cast<double>(found.n_elem)
                       / static_cast<double>(outcomes.n_elem);
        }

        return probs;
    }

    Col<double> find_optimal_split(const Mat<double> &data,
                                   const Col<uword> &outcomes,
                                   const Col<double> &weights,
                                   const Col<double> &thresholds,
                                   uword col,
                                   const Col<uword> &rows,
                                   double min_delta=0.1) {
        // Initialize some vars
        double prev_threshold(-datum::inf);
        double optimal_threshold(0.);
        double optimal_gain(0.);
        double gain(0.);

        // Compute the base entropy.
        const double base_entropy = shannonEntropy(probs_distribution(outcomes(rows)));

        for(uword i=0; i < thresholds.n_elem; i++) {
            if(thresholds[i] >= prev_threshold + min_delta) {
                gain = base_entropy;
                {
                    // Compute entropy of the element distribution below thresholds[i]
                    // and subtract it from gain
                    uvec below = rows(find(data(rows, uvec({col})) < thresholds[i]));
                    gain -=   sum(weights(below))
                            * shannonEntropy(probs_distribution(outcomes(below)))
                            / sum(weights(rows));
                }
                {
                    // Compute entropy of the element distribution above thresholds[i]
                    // and subtract it from gain
                    uvec above = rows(find(data(rows, uvec({col})) >= thresholds[i]));
                    gain -=   sum(weights(above))
                            * shannonEntropy(probs_distribution(outcomes(above)))
                            / sum(weights(rows));
                }

                // If we're better than the best, store this value as a candidate
                if(gain > optimal_gain) {
                    optimal_threshold = thresholds[i];
                    optimal_gain = gain;
                }

                // Store the current threshold so that we can honour min_delta
                prev_threshold = thresholds[i];
            }
        }

        return colvec({optimal_threshold, optimal_gain});
    }

    Col<double> find_cat_split_performance(const Mat<double> &data,
                                           const Col<uword> &outcomes,
                                           const Col<double> &weights,
                                           uword col,
                                           const Col<uword> &rows) {
        // The total gain (we'll subtract from this)
        double gain = shannonEntropy(probs_distribution(outcomes(rows)));
        Col<double> categories = unique(data(rows, uvec({col})));

        for(uword i=0; i < categories.n_elem; i++) {
            uvec category_elems = rows(find(data(rows, uvec({col})) == categories[i]));
            gain -=   sum(weights(category_elems))
                    * shannonEntropy(probs_distribution(outcomes(category_elems)))
                    / sum(weights(rows));
        }

        return colvec({datum::nan, gain});
    }

    // This functions takes input data and determines the set of filters
    // which provide the best split based on Shannon's entropy criteria.
    // The resulting filters are returned as a nlohmann::json object
    // This function will call itself recursively.
    json partition_data(const Mat<double> &data, const Col<uword> &outcomes,
                        const Col<double> &weights,
                        double min_delta, uint64_t max_levels,
                        Col<uword> &data_types,
                        Col<uword> ignore_col,
                        Col<uword> rows,
                        uint64_t level=0) {
        // Store unique results and their probabilities,
        // based on the info so-far
        Col<uword> possible_outcomes = unique(outcomes(rows));
        std::map<std::string, double> probs_map;
        Col<double> probs = probs_distribution(outcomes(rows));
        for(uword i=0; i < possible_outcomes.n_elem; i++) {
            probs_map[std::to_string(possible_outcomes[i])] = probs[i];
        }

        // Check if we should stop iterating
        if(possible_outcomes.n_elem < 2 || level >= max_levels) {
            return {{"probs", probs_map}};
        }

        // Determine the optimal split column and location
        Mat<double> split_performances = zeros<Mat<double>>(2, data.n_cols);
        #pragma omp parallel for
        for(uword i=0; i < data.n_cols; i++) {
            if(ignore_col[i] != 1) {
                if(data_types[i] == REAL) {
                    // unique returns unique elements, sorted in ascending order
                    Col<double> sorted_data = unique(data(rows, uvec({i})));
                    if(sorted_data.n_elem > 1) {
                        // Determine the thresholds as the midpoints in sorted_data
                        Col<double> thresholds = zeros<Col<double>>(sorted_data.n_elem - 1);
                        for(uword j=0; j < sorted_data.n_elem-1; j++) {
                            thresholds[j] = (sorted_data[j] + sorted_data[j+1]) / 2.0;
                        }
                        split_performances.col(i) = find_optimal_split(data,
                                                                       outcomes,
                                                                       weights,
                                                                       thresholds,
                                                                       i, rows,
                                                                       min_delta);
                    } else {
                        // Don't explore this column again
                        ignore_col[i] = 1;
                    }
                } else if(data_types[i] == ORDERED) {
                    Col<double> sorted_data = unique(data(rows, uvec({i})));
                    if(sorted_data.n_elem > 1) {
                        // All the points are thresholds, except the first one
                        Col<double> thresholds = zeros<Col<double>>(sorted_data.n_elem - 1);
                        for(uword j=0; j < sorted_data.n_elem-1; j++) {
                            thresholds[j] = sorted_data[j+1];
                        }
                        split_performances.col(i) = find_optimal_split(data,
                                                                       outcomes,
                                                                       weights,
                                                                       thresholds,
                                                                       i, rows,
                                                                       min_delta);
                    } else {
                        // Don't explore this column again
                        ignore_col[i] = 1;
                    }
                } else if(data_types[i] == CATEGORICAL) {
                    // No thresholds here, split by all the values
                    split_performances.col(i) = find_cat_split_performance(data,
                                                                           outcomes,
                                                                           weights,
                                                                           i, rows);
                }
            }
        }

        // No columns to explore? => Return since we're at the end of the tree branch
        uvec ignored_cols = find(ignore_col == 1);
        if(ignored_cols.n_elem == ignore_col.n_elem) {
            return {{"probs", probs_map}};
        }

        // Set index_max (in a cumbersome way)
        uword index_max = 0;
        split_performances.row(1).max(index_max);

        // Actually split data and iterate
        if(data_types[index_max] == CATEGORICAL) {
            ignore_col[index_max] = 1;

            // Analyze the data for each category separately
            json children;
            Col<double> categories = unique(data(rows, uvec({index_max})));
            for(uword i=0; i < categories.n_elem; i++) {
                Col<uword> filtered_rows = rows(find(data(rows, uvec({index_max})) == categories[i]));
                if(filtered_rows.n_elem > 0) {
                    children[std::to_string(i)] = partition_data(data,
                                                                 outcomes,
                                                                 weights,
                                                                 min_delta, max_levels,
                                                                 data_types, ignore_col,
                                                                 filtered_rows,
                                                                 level + 1);
                    children[std::to_string(i)]["filter_label"] = std::to_string(static_cast<int>(categories[i]));
                }
            }

            return {{"categories", categories},
                    {"column", index_max},
                    {"children", children},
                    {"probs", probs_map}};
        } else {
            // ORDERED or REAL
            // Iterate on the data above and below the threshold
            json children;
            double threshold = split_performances(0, index_max);
            uword i = 0;
            Col<uword> filtered_rows = rows(find(data(rows, uvec({index_max})) < threshold));
            if(filtered_rows.n_elem > 0) {
                children[std::to_string(i)] = partition_data(data,
                                               outcomes,
                                               weights,
                                               min_delta, max_levels, data_types,
                                               ignore_col,
                                               filtered_rows,
                                               level + 1);
                children[std::to_string(i)]["filter_label"] = "<" + std::to_string(threshold);
                i += 1;
            }
            filtered_rows = rows(find(data(rows, uvec({index_max})) >= threshold));
            if(filtered_rows.n_elem > 0) {
                children[std::to_string(i)] = partition_data(data,
                                               outcomes,
                                               weights,
                                               min_delta, max_levels, data_types,
                                               ignore_col,
                                               filtered_rows,
                                               level + 1);
                children[std::to_string(i)]["filter_label"] = ">=" + std::to_string(threshold);
            }

            return {{"threshold", threshold},
                    {"column", index_max},
                    {"children", children},
                    {"probs", probs_map}};
        }
    }

    // Create an Armadillo Matrix from a NumPy array of doubles and print it
    std::string train(pybind11::array_t<double> &trainingData_mat,
                      pybind11::array_t<uint64_t> &outcomes_vec,
                      pybind11::array_t<double> &weights_vec,
                      double min_delta, uint64_t max_levels,
                      pybind11::array_t<uint64_t> &criteria) {

        /* Create trainingData & outcomes matrices from Python buffer objects */
        /*
         * Since this is a 2D case, we must transpose, that means that:
         * trainingData_info.shape[1] -> nrows
         * trainingData_info.shape[0] -> ncols
        */
        pybind11::buffer_info trainingData_info = trainingData_mat.request();

        // Check that we got a 2D array
        if(trainingData_info.ndim != 2) {
            throw std::runtime_error("trainingData must be a 2D array");
        }

        // The buffer needs transposing
        Mat<double> trainingData = mat((double *)trainingData_info.ptr,
                                       trainingData_info.shape[1],
                                       trainingData_info.shape[0], true, true);
        inplace_trans(trainingData);

        pybind11::buffer_info outcomes_info = outcomes_vec.request();

        // Check that we got a 1D array
        if(outcomes_info.ndim != 1) {
            throw std::runtime_error("outcomes must be a 1D array");
        }

        // Check that the size of the list vector matches the number of data columns in "array"
        if(outcomes_info.shape[0] != trainingData.n_rows) {
            throw std::runtime_error("The number of elements in outcomes must "
                                     "match the number of rows in trainingData");
        }

        // uword == uint64_t only true under certain (common) conditions!
        // See http://arma.sourceforge.net/docs.html#uword
        Col<uword> outcomes = uvec((uword *)outcomes_info.ptr, outcomes_info.shape[0], true, true);

        pybind11::buffer_info weights_info = weights_vec.request();

        // Check that we got a 1D array
        if(weights_info.ndim != 1) {
            throw std::runtime_error("weights must be a 1D array");
        }

        // Check that the size of the list vector matches the number of data columns in "array"
        if(weights_info.shape[0] != trainingData.n_rows) {
            throw std::runtime_error("The number of elements in weights must "
                                     "match the number of rows in trainingData");
        }

        // uword == uint64_t only true under certain (common) conditions!
        // See http://arma.sourceforge.net/docs.html#uword
        Col<double> weights = vec((double *)weights_info.ptr, weights_info.shape[0], true, true);

        pybind11::buffer_info criteria_info = criteria.request();

        // Check that we got a 1D array
        if(criteria_info.ndim != 1) {
            throw std::runtime_error("criteria must be a 1D array");
        }

        // Check that the size of the list vector matches the number of data columns in "array"
        if(criteria_info.shape[0] != trainingData.n_cols) {
            throw std::runtime_error("The number of elements in criteria must "
                                     "match the number of columns in trainingData");
        }

        // uword == uint64_t only true under certain (common) conditions!
        // See http://arma.sourceforge.net/docs.html#uword
        Col<uword> data_types = uvec((uword *)criteria_info.ptr, criteria_info.shape[0], true, true);
        // A column vector with all the indices in trainingData,
        // since at first we want to consider all the rows
        Col<uword> indices = linspace<Col<uword>>(0, trainingData.n_rows-1, trainingData.n_rows);
        // A column vector indicating which columns should be ignored
        Col<uword> ignore_col = zeros<Col<uword>>(trainingData.n_cols);

        wall_clock timer;

        timer.tic();
        json tree = partition_data(trainingData, outcomes,
                                   weights,
                                   min_delta, max_levels,
                                   data_types, ignore_col,
                                   indices);
        double t = timer.toc();

        return tree.dump(4);
    }

}
