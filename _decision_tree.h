#pragma once

#ifdef _WIN32
#include <cmath>
#endif
#include "pybind11/numpy.h"
#include "pybind11/pybind11.h"

namespace _decision_tree {
    enum {
        REAL = 0,
        ORDERED,
        CATEGORICAL
    };

    extern std::string train(pybind11::array_t<double> &trainingData_mat,
                             pybind11::array_t<uint64_t> &outcomes_vec,
                             pybind11::array_t<double> &weights_vec,
                             double min_delta, uint64_t max_levels,
                             pybind11::array_t<uint64_t> &criteria);
}
