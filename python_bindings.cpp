#include "_decision_tree.h"
#include "pybind11/pybind11.h"

PYBIND11_PLUGIN(_decision_tree) {
    namespace py = pybind11;

    // Export the module
    py::module m("_decision_tree", "simple decision tree implementation in C++ with Armadillo & pybind11");

    // Export constants
    m.attr("REAL") = py::int_(0);
    m.attr("ORDERED") = py::int_(1);
    m.attr("CATEGORICAL") = py::int_(2);

    // Export methods
    m.def("train", &_decision_tree::train, "Train a decision tree",
          py::arg("trainingData"), py::arg("outcomes"), py::arg("weights"),
          py::arg("min_delta"), py::arg("max_levels"),
          py::arg("criteria"));

    return m.ptr();
}
