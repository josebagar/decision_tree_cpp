#!/usr/bin/env python3
import re
import sys
import json
import numpy as np
import pandas as pd
try:
    raise ImportError
    import _decision_tree
except ImportError:
    sys.stdout.write('\033[93mNOTICE: Using Python based decision tree implementation\033[0m\n')
    import _decision_tree_python as _decision_tree
from mathlas.object import MathlasObject
from mathlas.statistics.probabilisticChoiceObject import probabilisticChoiceObject


class DecisionTree(MathlasObject):
    """
    Decision tree implementation which can make use of a
    C++ implementation for faster tree training.
    """

    def __init__(self, trainingData, resultColumn,
                 weights=None, maxLevels=15, minDelta=0.2):
        # Some initial data/type checks
        if not isinstance(trainingData, pd.DataFrame):
            raise TypeError('trainingData must be a pandas DataFrame')

        if resultColumn not in trainingData.columns:
            raise ValueError('"{}" is not in trainingData columns'.format(resultColumn))

        # Set the weights vector
        if weights is None:
            weights = np.ones(trainingData.shape[0], dtype=np.double)
        elif isinstance(weights, (np.array, pd.Series, pd.DataFrame)):
            # weights is an array/Series/DataFrame -> make sure it's a 1D NumPy array
            # We don't check sizes: _decision_tree.train will do that for us
            weights = np.asarray(weights)
            if len(weights.shape) > 1:
                weights = np.ravel(weights)
        else:
            raise TypeError('weights must be a 1D array')

        # Store the trainingData and outcome columns
        self.dataColumns = [col for col in trainingData.columns if col != resultColumn]
        self.resultColumn = resultColumn

        # Split trainingData into a trainingData and outcomes matrices
        outcomes = trainingData[resultColumn]
        trainingData = trainingData[self.dataColumns]

        # Change all the non-numeric columns into categorical data columns,
        # then store the categories and change the column into numeric
        self.dataCategories = {}
        dataTypes = np.zeros(trainingData.shape[1])
        for i, col in enumerate(trainingData.columns):
            if trainingData[col].dtype in ['int32', 'int64']:
                dataTypes[i] = _decision_tree.ORDERED
            elif trainingData[col].dtype in ['float32', 'float64']:
                dataTypes[i] = _decision_tree.REAL
            else:
                # If ordered categorical -> set dataTypes[i] accordingly
                # If categorical but unordered -> set dataTypes[i] accordingly
                # If non-categorical (strings, mainly) -> convert to categorical and
                #                                         set dataTypes[i] accordingly
                if hasattr(trainingData[col], 'cat'):
                    dataTypes[i] = _decision_tree.CATEGORICAL
                    if trainingData[col].cat.ordered:
                        dataTypes[i] = _decision_tree.ORDERED
                else:
                    trainingData.loc[:, col] = trainingData[col].astype('category')
                    dataTypes[i] = _decision_tree.CATEGORICAL

                # Store the categories and convert the matrix data to integers
                self.dataCategories[col] = trainingData[col].cat.categories
                trainingData[col].cat.categories = range(len(self.dataCategories[col]))

        # The outcomes column is expected to be categorical,
        # since it corresponds to observations
        if not hasattr(outcomes, 'cat'):
            outcomes = outcomes.astype('category')

        self.outcomeCategories = outcomes.cat.categories
        outcomes.cat.categories = range(len(self.outcomeCategories))

        # We can now convert both trainingData and outcomes into float types
        trainingData = trainingData.values.astype(np.float64)
        outcomes = outcomes.values.astype(np.float64)

        # Read the tree filters from the C++ implementation as a JSON object
        self.filters = json.loads(_decision_tree.train(trainingData, outcomes,
                                                       weights,
                                                       minDelta, maxLevels,
                                                       dataTypes))

        # C++ only works with floats, but when categorizing data we want to deal
        # with pandas DataFrames which are expressed in the same manner as the
        # original data the user fed us with => We must postprocess the filter
        self._postProcessFilters(self.filters, dataTypes)

        # print(json.dumps(self.filters, indent=4))

    def categorize(self, orig_data, filters=None, data=None, store_values='raw'):
        """
        Categorize the entries in data
        The schema in "data" must match that in the trainingData used when
        constructing the tree.
        In case that some data can not be categorized, its resultColumn will
        contain ``None`` unless store_intermediate is set to True.

        Parameters
        ----------
        store_values : string
                - 'raw' to store either the value or the probabilistic object
                - 'majority' to store the majority vote
        """
        if filters is None:
            filters = self.filters

        if data is None:
            data = orig_data.copy()

        # Store zero-order model for the given data
        if store_values == 'raw':
            orig_data.loc[data.index, self.resultColumn] = \
                probabilisticChoiceObject(filters['probs'])
        else:
            orig_data.loc[data.index, self.resultColumn] = \
                probabilisticChoiceObject(filters['probs']).majority_vote

        # Now split the data and recurse
        if 'children' in filters.keys() :
            if 'categories' in filters.keys():
                # Data is categorical -> split by the categories present at training
                for i, cat in enumerate(filters['categories']):
                    self.categorize(orig_data, filters['children'][str(i)],
                                    data[data[filters['column']] == cat], store_values)
            else:
                # Data is ordered -> split by threshold
                self.categorize(orig_data, filters['children']['0'],
                                data[data[filters['column']] < filters['threshold']],
                                store_values)
                self.categorize(orig_data, filters['children']['1'],
                                data[data[filters['column']] >= filters['threshold']],
                                store_values)

    def plot(self):
        """
        Plot the decision tree using networkx, since we can then
        get a nice graphic.
        Requires NetWorkX as well as pyplot and pygraphviz.
        """
        try:
            import networkx as nx
            import matplotlib.pyplot as plt
        except ImportError:
            sys.stderr.write("ERROR: NetworkX&Pyplot are needed for plotting\n")
            return

        graph = nx.DiGraph()
        labels = {}
        # Start the iterative process that adds the nodes to the graph
        self._addChildrenToGraph(graph, self.filters, labels)
        edge_labels = dict(((u, v), str(d['label'])[:7]) for u, v, d in graph.edges(data=True))

        plt.figure()
        try:
            pos = nx.nx_agraph.graphviz_layout(graph, prog='dot')
        except ImportError:
            sys.stdout.write('\033[93mNOTICE: For a better looking diagram, '+
                             'install pygraphviz\033[0m\n')
            pos = nx.random_layout(graph)
        nx.draw_networkx_nodes(graph, pos, node_color='#e96d20', alpha=0.6)
        nx.draw_networkx_edges(graph, pos, arrows=False)
        nx.draw_networkx_labels(graph, pos, labels=labels,
                                font_color='#005288',
                                font_weight="bold")
        nx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels)
        plt.gca().axes.get_xaxis().set_visible(False)
        plt.gca().axes.get_yaxis().set_visible(False)
        plt.show()

    def _postProcessFilters(self, filters, dataTypes):
        """
        Iterate over the filters and convert values back to user-provided form
        """
        # Convert probabilities
        for key in list(filters['probs'].keys()):
            filters['probs'][self.outcomeCategories[int(key)]] = filters['probs'].pop(key)

        # Convert column name and iterate over children (if any)
        if 'children' in filters:
            nCol = int(filters['column'])
            filters['column'] = self.dataColumns[int(filters['column'])]

            # Convert the categories, too
            if dataTypes[nCol] in (_decision_tree.CATEGORICAL, _decision_tree.ORDERED):
                # Convert filters['categories'] back into original values, if provided
                if filters['column'] in self.dataCategories:
                    filters['categories'] = [self.dataCategories[filters['column']][int(n)]
                                             for n in filters['categories']]

            # Change the children's filter_label fields
            for i in range(len(filters['children'])):
                key = sorted(filters['children'].keys())[i]
                childFilter = filters['children'][key]
                if dataTypes[nCol] == _decision_tree.CATEGORICAL:
                    n = int(childFilter['filter_label'])
                    childFilter['filter_label'] = self.dataCategories[filters['column']][n]
                elif dataTypes[nCol] == _decision_tree.ORDERED:
                    # TODO: Ordered categorical is untested as of now
                    sys.stdout.write("\033[93mWARNING: Ordered categorical data is untested as "+
                                     "of now\033[0m\n")
                    if filters['column'] in self.dataCategories:
                        res = re.search('\A[<>]=?', childFilter['filter_label'])
                        if res:
                            n = int(childFilter['filter_label'][res.end():])
                            childFilter['filter_label'] = '{}{}'.format(childFilter['filter_label']
                                                                        [:res.end()],
                                                                        self.dataCategories[filters['column']][n])
                self._postProcessFilters(childFilter, dataTypes)

    def _addChildrenToGraph(self, graph, filters, dictLabels, name="0"):
        """
        Will add node "parent" and its children to the given graph.
        This is an auxiliary method to self.plot()
        """
        # Add the parent to the graph
        graph.add_node(name)

        # If parent has children, add them
        if "children" in filters:
            dictLabels[name] = filters["column"]
            for i in range(len(filters['children'])):
                key = sorted(filters['children'].keys())[i]
                childFilters = filters['children'][key]
                childName = '_'.join((name, key))
                self._addChildrenToGraph(graph, childFilters, dictLabels, childName)
                graph.add_edge(name, childName, label=filters['children'][key]['filter_label'])
        else:
            dictLabels[name] = ', '.join(['{:.1f}% {}'.format(100 * val, key) for key, val in filters['probs'].items()])


def main():
    import time
    from misc.sickness_database import sickness_database

    try:
        fdb = pd.read_pickle('fdb.pickle')
        print("DB read from cache")
    except:
        symptoms = {"temperature": ["float", [36., 40.],
                                    lambda x, a, b: a + (b - a) * 6 * (0.5 * x ** 2 - (x ** 3) / 3.)],
                    "headache": ["categoric", ["yes", "no"], [0.3, 0.7]],
                    "vomits": ["categoric", ["never", "sometimes", "often"], [0.5, 0.4, 0.1]],
                    "mucus": ["categoric", ["yes", "no"], [0.6, 0.4]]}

        # **** Definition of the sicknesses ****
        # Each sickness has a list os values it can take.
        sickness = {"flu": ["yes", "no"]}

        # **** List of trees that define how the we determine the resulting sickness ****
        # Description of the underlying decision tree
        schema = {"flu": {"temperature": {"<37": "no",
                                          ">=37:<38": {"headache": {"no": {"mucus": {"yes": "yes",
                                                                                     "no": "no"}},
                                                                    "yes": "yes"}},
                                          ">=38:<39": {"mucus": {"yes": "no",
                                                                 "no": "yes"}},
                                          ">=39": {"mucus": {"yes": "no",
                                                             "no": "yes"}}}}}

        n = 100000
        t0 = time.time()
        fdb = sickness_database(n, symptoms, sickness, schema)
        t1 = time.time()
        fdb.to_pickle('fdb.pickle')
        print('DB generated in {:.2f}s'.format(t1-t0))
    # t0 = time.time()
    # dt = DT(fdb, 'flu 0.0', minDelta=0.1, maxLevels=5)
    # t1 = time.time()
    # sys.stdout.write("Old implementation took {:.2f}s\n".format(t1-t0))
    #
    # fdb2 = fdb.copy()
    # dt.categorize(fdb2)
    # pythonResults = fdb2['flu 0.0']
    #
    # fdb2.to_csv('/home/joseba/tmp/oldResults.csv')

    # Now convert the data for new implementation processing
    t0 = time.time()
    dt = DecisionTree(fdb, 'flu 0.0', minDelta=0.1, maxLevels=5)
    t1 = time.time()
    sys.stdout.write("New implementation took {:.5f}s (from wrapper side)\n".format(t1-t0))

    fdb2 = fdb.copy()
    dt.categorize(fdb2)

    cResults = fdb2['flu 0.0']

    fdb2.to_csv('/home/joseba/tmp/newResults.csv')

    # print('Success?: {}'.format((pythonResults == cResults).all()))

    return 0

if __name__ == '__main__':
    sys.exit(main())

